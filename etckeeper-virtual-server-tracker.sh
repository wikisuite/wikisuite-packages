#!/usr/bin/env sh

# This script is used to track configuration changes using Etckeeper before and
# after modifying a Virtualmin virtual server.

# Parse command line arguments
STAGE="pre"  # default value
for arg in "$@"; do
    case "$arg" in
        --pre)
        STAGE="pre"
        ;;
        --post)
        STAGE="post"
        ;;
    esac
done

# Adjust commit message based on stage
if [ "$STAGE" = "pre" ]; then
    COMMIT_MESSAGE="Virtualmin API: Before '$VIRTUALSERVER_DOM' modification"
else
    COMMIT_MESSAGE="Virtualmin API: After '$VIRTUALSERVER_DOM' modification"
fi

# First stage all changes (even if none)
if ! ERROR=$(etckeeper vcs add -A 2>&1); then
    echo "Failed to stage changes with Etckeeper, $ERROR" >&2
    exit 1
fi

# Force commit even if no changes
if ! ERROR=$(etckeeper vcs commit --allow-empty -m "$COMMIT_MESSAGE" 2>&1); then
    echo "Failed to commit changes with Etckeeper, $ERROR" >&2
    exit 1
fi

exit 0
